% $$FileInfo
% $Filename: Newton.m
%
% Copyright (c) 2014-2015 Jelena Banjac.
%

function [ f, Fun ] = Newton(x,y,z,d,satellitesNo,matOfVisSatelites)
% Funkcija prima podatke iz prethodne iteracije:
%     (x,y,z) - polozaj resivera dobijen u prethodnoj iteraciji
%               i obavlja narednu.
%     matOfVisSatelites - Matrix of coordinates of visible satellites
%     satellitesNo - Number of visible satellites
c = 3*10^8;

%satellitesNo = 4;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for i=1:satellitesNo
    X = matOfVisSatelites(:,1);
    X = X';
    Y = matOfVisSatelites(:,2);
    Y = Y';
    Z = matOfVisSatelites(:,3);
    Z = Z';
    T = matOfVisSatelites(:,4);
    T = T';
    T = T./c;
end
% Podaci 4 satelita pomocu kojih odredjujemo poziciju resivera
% Omoguceno je da broj satelita bude i veci, a za sada je testiran sa 4.
% Podaci bi se citali iz neke datoteke
% X(1) = 7766188.44;
% Y(1) = -21960535.34;
% Z(1) = 12522838.56;
% T(1) = 1022228206.42/c;
%
% X(2) = -25922679.66;
% Y(2) = -6629461.28;
% Z(2) = 31864.37;
% T(2) = 1024096139.11/c;
%
% X(3) = -5743774.02;
% Y(3) = -25828319.92;
% Z(3) = 1692757.72;
% T(3) = 1021729070.63/c;
%
% X(4) = -2786005.69;
% Y(4) = -15900725.8;
% Z(4) = 21302003.49;
% T(4) = 1021259581.09/c;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Inicijalna pretpostavka lokacije resivera
x0 = [x; y; z; d];

% Jednacine sfera koje opisuju poziciju svakog vidljivog satelita
% F1 = (x-X(1))^2 + (y-Y(1))^2 + (z-Z(1))^2 - (c * (T(1)-d))^2;
% F2 = (x-X(2))^2 + (y-Y(2))^2 + (z-Z(2))^2 - (c * (T(2)-d))^2;
% F3 = (x-X(3))^2 + (y-Y(3))^2 + (z-Z(3))^2 - (c * (T(3)-d))^2;
% F4 = (x-X(4))^2 + (y-Y(4))^2 + (z-Z(4))^2 - (c * (T(4)-d))^2;
for i=1:satellitesNo
    F(i) = (x - X(i))^2 + (y - Y(i))^2 + (z - Z(i))^2 - (c * (T(i) - d))^2;
end

% Jednacine koje opisuju Jakobija, matricu parcijalnih izvoda
% F1x = 2 * (x-X(1));
% F1y = 2 * (y-Y(1));
% F1z = 2 * (z-Z(1));
% F1d = 2 * c^2 * (T(1)-d);
%
% F2x = 2 * (x-X(2));
% F2y = 2 * (y-Y(2));
% F2z = 2 * (z-Z(2));
% F2d = 2 * c^2 * (T(2)-d);
%
% F3x = 2 * (x-X(3));
% F3y = 2 * (y-Y(3));
% F3z = 2 * (z-Z(3));
% F3d = 2 * c^2 * (T(3)-d);
%
% F4x = 2 * (x-X(4));
% F4y = 2 * (y-Y(4));
% F4z = 2 * (z-Z(4));
% F4d = 2 * c^2 * (T(4)-d);

% dFun predstavlja Jakobijevu matricu
% dFun = [F1x F1y F1z F1d; F2x F2y F2z F2d; F3x F3y F3z F3d; F4x F4y F4z F4d];
dFun = [];
for i=1:satellitesNo
    dFun = [dFun; 2 * (x - X(i)) 2 * (y - Y(i)) 2 * (z - Z(i)) 2 * c^2 * (T(i) - d)];
end


% Fun matrica sadrzi jednacine sfera
%Fun = [F(1); F(2); F(3); F(4)];
Fun = [];
for i=1:satellitesNo
    Fun = [Fun; F(i)];
end

% Sad obavljamo jednu iteraciju Njutnovog metoda
s =  dFun \ -Fun;
f = x0 + s;
end
