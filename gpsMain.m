% $$FileInfo
% $Filename: gpsMain.m
%
% Copyright (c) 2014-2015 Jelena Banjac.
%


fh = figure('DoubleBuffer','on'); % Sprecava treprenje slike prilikom redraw-a
ah = axes('Parent',fh,'Units','normalized','Position',[0 0 1 1],...
      'DataAspectRatio',[1 1 1],'DrawMode','fast'); % Koordinatne ose
format ('long') % Format brojeva prilikom ispisa u konzolu

% Koordinate GPS resivera
x1gps = linspace(-4, 4, 40);
x2gps = linspace(4, -4, 40);
Xgps = 1.0e+003 *[x1gps x2gps];
Zgps = 1.0e+003 *5.6;
Ygps = 1.0e+003 *[-sqrt(6.7^2-x1gps.^2-Zgps.^2) sqrt(6.7^2-x2gps.^2-Zgps.^2) ];
%   fprintf('\nSet s')
%   prompt1 = 'What is the original value? ';
%   result = input(prompt)

% Koordinate na osnovu kojih se dobijaju orbite GPS satelita
x1 = linspace(-27.5, 27.5, 40);
x2 = linspace(27.5, -27.5, 40);
x = 1.0e+003 *[x1 x2];
y = 1.0e+003 *[sqrt(27.5^2-x1.^2) -sqrt(27.5^2-x2.^2)];
z = zeros(size(x));


% Formiranje Zemlje u 3D prostoru
load('topo.mat','topo','topomap1');
[x1,y1,z1] = sphere(50);
x1 = 6678.14*x1;
y1 = 6678.14*y1;
z1 = 6678.14*z1;
props.AmbientStrength = 0.1;
props.DiffuseStrength = 1;
props.SpecularColorReflectance = .5;
props.SpecularExponent = 20;
props.SpecularStrength = 1;
props.FaceColor= 'texture';
%   props.FaceColor= 'texturemap';
props.EdgeColor = 'none';
props.FaceLighting = 'phong';
props.Cdata = topo;

sh = surface(x1,y1,z1,props); % Kasnije sluzi za rotaciju Zemljine povrsine
%   light('position',[-1 0 1]);
%   light('position',[-1.5 0.5 -0.5], 'color', [.6 .2 .2]);
view(3); % Ugao vidljivosti u 3D prostoru

% Definise izgled orbite
handles.p1 = line('parent',ah,'XData',x(1),'YData',y(1),'ZData',...
  z(1),'Color','blue','LineWidth',2);
% Definise izgled satelita
handles.p2 = line('parent',ah,'XData',x(end),'YData',y(end),...
  'ZData',z(end),'Marker','o','MarkerSize',6,'MarkerFaceColor','r');
% Koeficijent za medjusobnu udaljenost satelina sa jedne orbite
q = 20;

% Koordinate koje se prikazuju prilikom simulacije, za laksu orijentaciju
oaxes([0 0 0],'Arrow','extend','AxisLabelLocation','side',...
  'Xcolor','green','Ycolor','green','Zcolor','green');
%   set(oaxes,'Visible','off');
axis vis3d equal;
%   set(axis, 'BackgroundColor','black');
handles.XLim = get(gca,'XLim');
handles.YLim = get(gca,'YLim');
handles.ZLim = get(gca,'ZLim');
set([handles.p1,handles.p2],'Visible','off');
%   xmin = handles.XLim(1);
%   ymin = handles.YLim(1);
%   zmin = handles.ZLim(1);
%   xmax = handles.XLim(2);
%   ymax = handles.YLim(2);
%   zmax = handles.ZLim(2);
set(ah, 'XLim', [-27*1.0e+003  1.0e+003 *27],'YLim', [-27*1.0e+003 1.0e+003 *27],'Zlim',[-27*1.0e+003 1.0e+003 *27]);

%   set(ah, 'BackgroundColor', [1,1,1]);
view(3);

% Definise izgled GPS resivera
handles.receiver = line('parent',ah,'XData',Xgps(1), 'YData',Ygps(1),...
  'ZData',Zgps(1),'Marker','o', 'MarkerSize',10,'MarkerFaceColor','b');

%   handles.hsat1 = line('parent',ah,'XData',x(1), 'YData',y(1),...
%       'ZData',z(1),'Marker','o', 'MarkerSize',6,'MarkerFaceColor','r');
%   handles.hsat2 = line('parent',ah,'XData',x(1), 'YData',y(1),...
%       'ZData',z(1),'Marker','o', 'MarkerSize',6,'MarkerFaceColor','r');
%   handles.hsat3 = line('parent',ah,'XData',x(1), 'YData',y(1),...
%       'ZData',z(1),'Marker','o', 'MarkerSize',6,'MarkerFaceColor','r');
%   handles.hsat4 = line('parent',ah,'XData',x(1), 'YData',y(1),...
%       'ZData',z(1),'Marker','o', 'MarkerSize',6,'MarkerFaceColor','r');
%   handles.hsat5 = line('parent',ah,'XData',x(1), 'YData',y(1),...
%       'ZData',z(1),'Marker','o', 'MarkerSize',6,'MarkerFaceColor','r');
%   handles.hsat6 = line('parent',ah,'XData',x(1), 'YData',y(1),...
%       'ZData',z(1),'Marker','o', 'MarkerSize',6,'MarkerFaceColor','r');
%
%    handles.hsat7 = line('parent',ah,'XData',x(1), 'YData',y(1),...
%        'ZData',z(1),'Marker','o', 'MarkerSize',6,'MarkerFaceColor','r');
%   handles.hsat8 = line('parent',ah,'XData',x(1), 'YData',y(1),...
%       'ZData',z(1),'Marker','o', 'MarkerSize',6,'MarkerFaceColor','r');
%   handles.hsat9 = line('parent',ah,'XData',x(1), 'YData',y(1),...
%       'ZData',z(1),'Marker','o', 'MarkerSize',6,'MarkerFaceColor','r');
%   handles.hsat10 = line('parent',ah,'XData',x(1), 'YData',y(1),...
%       'ZData',z(1),'Marker','o', 'MarkerSize',6,'MarkerFaceColor','r');
%   handles.hsat11 = line('parent',ah,'XData',x(1), 'YData',y(1),...
%       'ZData',z(1),'Marker','o', 'MarkerSize',6,'MarkerFaceColor','r');
%   handles.hsat12 = line('parent',ah,'XData',x(1), 'YData',y(1),...
%       'ZData',z(1),'Marker','o', 'MarkerSize',6,'MarkerFaceColor','r');
%
%   handles.hsat13 = line('parent',ah,'XData',x(1), 'YData',y(1),...
%        'ZData',z(1),'Marker','o', 'MarkerSize',6,'MarkerFaceColor','r');
%   handles.hsat14 = line('parent',ah,'XData',x(1), 'YData',y(1),...
%       'ZData',z(1),'Marker','o', 'MarkerSize',6,'MarkerFaceColor','r');
%   handles.hsat15 = line('parent',ah,'XData',x(1), 'YData',y(1),...
%       'ZData',z(1),'Marker','o', 'MarkerSize',6,'MarkerFaceColor','r');
%   handles.hsat16 = line('parent',ah,'XData',x(1), 'YData',y(1),...
%       'ZData',z(1),'Marker','o', 'MarkerSize',6,'MarkerFaceColor','r');
%   handles.hsat17 = line('parent',ah,'XData',x(1), 'YData',y(1),...
%       'ZData',z(1),'Marker','o', 'MarkerSize',6,'MarkerFaceColor','r');
%   handles.hsat18 = line('parent',ah,'XData',x(1), 'YData',y(1),...
%       'ZData',z(1),'Marker','o', 'MarkerSize',6,'MarkerFaceColor','r');
%
%   handles.hsat19 = line('parent',ah,'XData',x(1), 'YData',y(1),...
%        'ZData',z(1),'Marker','o', 'MarkerSize',6,'MarkerFaceColor','r');
%   handles.hsat20 = line('parent',ah,'XData',x(1), 'YData',y(1),...
%       'ZData',z(1),'Marker','o', 'MarkerSize',6,'MarkerFaceColor','r');
%   handles.hsat21 = line('parent',ah,'XData',x(1), 'YData',y(1),...
%       'ZData',z(1),'Marker','o', 'MarkerSize',6,'MarkerFaceColor','r');
%   handles.hsat22 = line('parent',ah,'XData',x(1), 'YData',y(1),...
%       'ZData',z(1),'Marker','o', 'MarkerSize',6,'MarkerFaceColor','r');
%   handles.hsat23 = line('parent',ah,'XData',x(1), 'YData',y(1),...
%       'ZData',z(1),'Marker','o', 'MarkerSize',6,'MarkerFaceColor','r');
%   handles.hsat24 = line('parent',ah,'XData',x(1), 'YData',y(1),...
%       'ZData',z(1),'Marker','o', 'MarkerSize',6,'MarkerFaceColor','r');
%

% Definise pocetne polozaje svakog od satelita
handles.hsatsList = [];
for j=1:24
  handles.hsatsList(j) = line('parent',ah,'XData',x(1), 'YData',y(1),...
  'ZData',z(1),'Marker','o', 'MarkerSize',6,'MarkerFaceColor','r');
end

% Koeficijenti ki se koriste za odredjivanje razmaka izmedju satelita na
% jednoj orbiti
%   q1 = q + 1;
k = 2;
k1 = q+k-1;
%   k1 = k+7;
k2 = 2*q+k-1;
%   k2 = k1+23;
k3 = 3*q+k-1;
%   k3 = k2+27;

k4 = 8;
%   k4 = 5;
k5 = q+k4-1;
%   k5 = k4+7;
k6 = 2*q+k4-1;
%   k6 = k5+23;
k7 = 3*q+k4-1;
%   k7 = k6+27;

k8 = 13;
%   k8 = 8;
k9 = q+k8-1;
%   k9 = k8+7;
k10 = 2*q+k8-1;
%   k10 = k9+23;
k11 = 3*q+k8-1;
%   k11 = k10+27;

k12 = 17;
%   k12 = 11;
k13 = q+k12-1;
%   k13 = k12+7;
k14 = 2*q+k12-1;
%   k14 = k13+23;
k15 = 3*q+k12-1;
%   k15 = k14+27;

k16 = 19;
%   k16 = 14;
k17 = q+k16-1;
%   k17 = k16+7;
k18 = 2*q+k16-1;
%   k18 = k17+23;
k19 = 3*q+k16-1;
%   k19 = k18+27;

k20 = 22;
%   k20 = 17;
k21 = q+k20-1;
%   k21 = k20+7;
k22 = 2*q+k20-1;
%   k22 = k21+23;
k23 = mod(3*q+k20-1,80)+3;
%   k23 = k22+27;

u2 =length(x);
sateliteNumber = 24;
for ii=1:sateliteNumber
    % Definisanje linija koje spajaju sateliti i resiver
    linija(ii) = line([0 0],[0 0],[0 0],...
            'Color','green','LineWidth',1,'LineStyle','-');
end

alfa = 1/sqrt(2);
%   alfa=sqrt(3)/2;
while true
    %ORBITA 1
    handles.htray(k) = line([x(k-1) x(k)],[y(k-1) y(k)],[z(k-1) z(k)],...
        'Color','blue','LineWidth',1);
    set(handles.hsatsList(1),'XData',x(k),'YData',y(k),'ZData',z(k));
    handles.htray(k) = line([x(k1-1) x(k1)],[y(k1-1) y(k1)],[z(k1-1) z(k1)],...
        'Color','blue','LineWidth',1);
    set(handles.hsatsList(7),'XData',x(k1),'YData',y(k1),'ZData',z(k1));
    handles.htray(k) = line([x(k2-1) x(k2)],[y(k2-1) y(k2)],[z(k2-1) z(k2)],...
        'Color','blue','LineWidth',1);
    set(handles.hsatsList(13),'XData',x(k2),'YData',y(k2),'ZData',z(k2));
    handles.htray(k) = line([x(k3-1) x(k3)],[y(k3-1) y(k3)],[z(k3-1) z(k3)],...
        'Color','blue','LineWidth',1);
    set(handles.hsatsList(19),'XData',x(k3),'YData',y(k3),'ZData',z(k3));

    %ORBITA 2
    handles.htray(k) = line([z(k4-1) z(k4)],[y(k4-1) y(k4)],[x(k4-1) x(k4)],...
        'Color','blue','LineWidth',1);
    set(handles.hsatsList(2),'XData',z(k4),'YData',y(k4),'ZData',x(k4));
    handles.htray(k) = line([z(k5-1) z(k5)],[y(k5-1) y(k5)],[x(k5-1) x(k5)],...
        'Color','blue','LineWidth',1);
    set(handles.hsatsList(8),'XData',z(k5),'YData',y(k5),'ZData',x(k5));
    handles.htray(k) = line([z(k6-1) z(k6)],[y(k6-1) y(k6)],[x(k6-1) x(k6)],...
        'Color','blue','LineWidth',1);
    set(handles.hsatsList(14),'XData',z(k6),'YData',y(k6),'ZData',x(k6));
    handles.htray(k) = line([z(k7-1) z(k7)],[y(k7-1) y(k7)],[x(k7-1) x(k7)],...
        'Color','blue','LineWidth',1);
    set(handles.hsatsList(20),'XData',z(k7),'YData',y(k7),'ZData',x(k7));

    %ORBITA 3
    handles.htray(k) = line([alfa*x(k8-1) alfa*x(k8)],[y(k8-1) y(k8)],[alfa*x(k8-1) alfa*x(k8)],...
        'Color','blue','LineWidth',1);
    set(handles.hsatsList(3),'XData',alfa*x(k8),'YData',y(k8),'ZData',alfa*x(k8));
    handles.htray(k) = line([alfa*x(k9-1) alfa*x(k9)],[y(k9-1) y(k9)],[alfa*x(k9-1) alfa*x(k9)],...
        'Color','blue','LineWidth',1);
    set(handles.hsatsList(9),'XData',alfa*x(k9),'YData',y(k9),'ZData',alfa*x(k9));
    handles.htray(k) = line([alfa*x(k10-1) alfa*x(k10)],[y(k10-1) y(k10)],[alfa*x(k10-1) alfa*x(k10)],...
        'Color','blue','LineWidth',1);
    set(handles.hsatsList(15),'XData',alfa*x(k10),'YData',y(k10),'ZData',alfa*x(k10));
    handles.htray(k) = line([alfa*x(k11-1) alfa*x(k11)],[y(k11-1) y(k11)],[alfa*x(k11-1) alfa*x(k11)],...
        'Color','blue','LineWidth',1);
    set(handles.hsatsList(21),'XData',alfa*x(k11),'YData',y(k11),'ZData',alfa*x(k11));


    %ORBITA 4
    handles.htray(k) = line([-alfa*x(k12-1) -alfa*x(k12)],[y(k12-1) y(k12)],[alfa*x(k12-1) alfa*x(k12)],...
        'Color','blue','LineWidth',1);
    set(handles.hsatsList(4),'XData',-alfa*x(k12),'YData',y(k12),'ZData',alfa*x(k12));
    handles.htray(k) = line([-alfa*x(k13-1) -alfa*x(k13)],[y(k13-1) y(k13)],[alfa*x(k13-1) alfa*x(k13)],...
        'Color','blue','LineWidth',1);
    set(handles.hsatsList(10),'XData',-alfa*x(k13),'YData',y(k13),'ZData',alfa*x(k13));
    handles.htray(k) = line([-alfa*x(k14-1) -alfa*x(k14)],[y(k14-1) y(k14)],[alfa*x(k14-1) alfa*x(k14)],...
        'Color','blue','LineWidth',1);
    set(handles.hsatsList(16),'XData',-alfa*x(k14),'YData',y(k14),'ZData',alfa*x(k14));
    handles.htray(k) = line([-alfa*x(k15-1) -alfa*x(k15)],[y(k15-1) y(k15)],[alfa*x(k15-1) alfa*x(k15)],...
        'Color','blue','LineWidth',1);
    set(handles.hsatsList(22),'XData',-alfa*x(k15),'YData',y(k15),'ZData',alfa*x(k15));

    %ORBITA 5
    handles.htray(k) = line([y(k16-1) y(k16)],[alfa*x(k16-1) alfa*x(k16)],[alfa*x(k16-1) alfa*x(k16)],...
        'Color','blue','LineWidth',1);
    set(handles.hsatsList(5),'XData',y(k16),'YData',alfa*x(k16),'ZData',alfa*x(k16));
    handles.htray(k) = line([y(k17-1) y(k17)],[alfa*x(k17-1) alfa*x(k17)],[alfa*x(k17-1) alfa*x(k17)],...
        'Color','blue','LineWidth',1);
    set(handles.hsatsList(11),'XData',y(k17),'YData',alfa*x(k17),'ZData',alfa*x(k17));
    handles.htray(k) = line([y(k18-1) y(k18)],[alfa*x(k18-1) alfa*x(k18)],[alfa*x(k18-1) alfa*x(k18)],...
        'Color','blue','LineWidth',1);
    set(handles.hsatsList(17),'XData',y(k18),'YData',alfa*x(k18),'ZData',alfa*x(k18));
    handles.htray(k) = line([y(k19-1) y(k19)],[alfa*x(k19-1) alfa*x(k19)],[alfa*x(k19-1) alfa*x(k19)],...
        'Color','blue','LineWidth',1);
    set(handles.hsatsList(23),'XData',y(k19),'YData',alfa*x(k19),'ZData',alfa*x(k19));

    %ORBITA 6
    handles.htray(k) = line([y(k20-1) y(k20)],[-alfa*x(k20-1) -alfa*x(k20)],[alfa*x(k20-1) alfa*x(k20)],...
        'Color','blue','LineWidth',1);
    set(handles.hsatsList(6),'XData',y(k20),'YData',-alfa*x(k20),'ZData',alfa*x(k20));
    handles.htray(k) = line([y(k21-1) y(k21)],[-alfa*x(k21-1) -alfa*x(k21)],[alfa*x(k21-1) alfa*x(k21)],...
        'Color','blue','LineWidth',1);
    set(handles.hsatsList(12),'XData',y(k21),'YData',-alfa*x(k21),'ZData',alfa*x(k21));
    handles.htray(k) = line([y(k22-1) y(k22)],[-alfa*x(k22-1) -alfa*x(k22)],[alfa*x(k22-1) alfa*x(k22)],...
        'Color','blue','LineWidth',1);
    set(handles.hsatsList(18),'XData',y(k22),'YData',-alfa*x(k22),'ZData',alfa*x(k22));
    handles.htray(k) = line([y(k23-1) y(k23)],[-alfa*x(k23-1) -alfa*x(k23)],[alfa*x(k23-1) alfa*x(k23)],...
        'Color','blue','LineWidth',1);
    set(handles.hsatsList(24),'XData',y(k23),'YData',-alfa*x(k23),'ZData',alfa*x(k23));

    %drawnow;
    k = k + 1;
    k1 = k1 + 1;
    k2 = k2 + 1;
    k3 = k3 + 1;
    k4 = k4 + 1;
    k5 = k5 + 1;
    k6 = k6 + 1;
    k7 = k7 + 1;
    k8 = k8 + 1;
    k9 = k9 + 1;
    k10 = k10 + 1;
    k11 = k11 + 1;
    k12 = k12 + 1;
    k13 = k13 + 1;
    k14 = k14 + 1;
    k15 = k15 + 1;
    k16 = k16 + 1;
    k17 = k17 + 1;
    k18 = k18 + 1;
    k19 = k19 + 1;
    k20 = k20 + 1;
    k21 = k21 + 1;
    k22 = k22 + 1;
    k23 = k23 + 1;

    %1. satelit u svakoj od odrbita (k, k4, k8, k12, k16)
    if k==u2
        handles.htray(k) = line([x(u2-1) x(1)],[y(u2-1) y(1)],[z(u2-1) z(1)],...
            'Color','blue','LineWidth',1);
        set(handles.hsatsList(1),'XData',x(1),'YData',y(1),'ZData',z(1));
        k = 2;
    end
    if k4==u2
        handles.htray(k) = line([z(u2-1) z(1)],[y(u2-1) y(1)],[x(u2-1) x(1)],...
            'Color','blue','LineWidth',1);
        set(handles.hsatsList(2),'XData',z(1),'YData',y(1),'ZData',x(1));
        k4 = 2;
    end
    if k8==u2
        handles.htray(k) = line([alfa*x(u2-1) alfa*x(1)],[y(u2-1) y(1)],[alfa*x(u2-1) alfa*x(1)],...
            'Color','blue','LineWidth',1);
        set(handles.hsatsList(3),'XData',alfa*x(k),'YData',y(k),'ZData',alfa*x(k));
        k8 = 2;
    end
    if k12==u2
        handles.htray(k) = line([-alfa*x(u2-1) -alfa*x(1)],[y(u2-1) y(1)],[alfa*x(u2-1) alfa*x(1)],...
            'Color','blue','LineWidth',1);
        set(handles.hsatsList(4),'XData',-alfa*x(1),'YData',y(1),'ZData',alfa*x(1));
        k12 = 2;
    end
    if k16==u2
        handles.htray(k) = line([y(u2-1) y(1)],[alfa*x(u2-1) alfa*x(1)],[alfa*x(u2-1) alfa*x(1)],...
            'Color','blue','LineWidth',1);
        set(handles.hsatsList(5),'XData',y(1),'YData',alfa*x(1),'ZData',alfa*x(1));
        k16 = 2;
    end
    if k20==u2
        handles.htray(k) = line([y(u2-1) y(1)],[-alfa*x(u2-1) -alfa*x(1)],[alfa*x(u2-1) alfa*x(1)],...
            'Color','blue','LineWidth',1);
        set(handles.hsatsList(6),'XData',y(1),'YData',-alfa*x(1),'ZData',alfa*x(1));
        k20 = 2;
    end

    %2. satelit u svakoj od odrbita (k1, k5, k9, k13, k17)
    if k1==u2
        handles.htray(k) = line([x(u2-1) x(1)],[y(u2-1) y(1)],[z(u2-1) z(1)],...
            'Color','blue','LineWidth',1);
        set(handles.hsatsList(7),'XData',x(1),'YData',y(1),'ZData',z(1));
        k1 = 2;
    end
    if k5==u2
        handles.htray(k) = line([z(u2-1) z(1)],[y(u2-1) y(1)],[x(u2-1) x(1)],...
            'Color','blue','LineWidth',1);
        set(handles.hsatsList(8),'XData',z(1),'YData',y(1),'ZData',x(1));
        k5 = 2;
    end
    if k9==u2
        handles.htray(k) = line([alfa*x(u2-1) alfa*x(1)],[y(u2-1) y(1)],[alfa*x(u2-1) alfa*x(1)],...
            'Color','blue','LineWidth',1);
        set(handles.hsatsList(9),'XData',alfa*x(k),'YData',y(k),'ZData',alfa*x(k));
        k9 = 2;
    end
    if k13==u2
        handles.htray(k) = line([-alfa*x(u2-1) -alfa*x(1)],[y(u2-1) y(1)],[alfa*x(u2-1) alfa*x(1)],...
            'Color','blue','LineWidth',1);
        set(handles.hsatsList(10),'XData',-alfa*x(1),'YData',y(1),'ZData',alfa*x(1));
        k13 = 2;
    end
    if k17==u2
        handles.htray(k) = line([y(u2-1) y(1)],[alfa*x(u2-1) alfa*x(1)],[alfa*x(u2-1) alfa*x(1)],...
            'Color','blue','LineWidth',1);
        set(handles.hsatsList(11),'XData',y(1),'YData',alfa*x(1),'ZData',alfa*x(1));
        k17 = 2;
    end
    if k21==u2
        handles.htray(k) = line([y(u2-1) y(1)],[-alfa*x(u2-1) -alfa*x(1)],[alfa*x(u2-1) alfa*x(1)],...
            'Color','blue','LineWidth',1);
        set(handles.hsatsList(12),'XData',y(1),'YData',-alfa*x(1),'ZData',alfa*x(1));
        k21 = 2;
    end

    %3. satelit u svakoj od odrbita (k2, k6, k10, k14, k18)
    if k2==u2
        handles.htray(k) = line([x(u2-1) x(1)],[y(u2-1) y(1)],[z(u2-1) z(1)],...
            'Color','blue','LineWidth',1);
        set(handles.hsatsList(13),'XData',x(1),'YData',y(1),'ZData',z(1));
        k2 = 2;
    end
    if k6==u2
        handles.htray(k) = line([z(u2-1) z(1)],[y(u2-1) y(1)],[x(u2-1) x(1)],...
            'Color','blue','LineWidth',1);
        set(handles.hsatsList(14),'XData',z(1),'YData',y(1),'ZData',x(1));
        k6 = 2;
    end
    if k10==u2
        handles.htray(k) = line([alfa*x(u2-1) alfa*x(1)],[y(u2-1) y(1)],[alfa*x(u2-1) alfa*x(1)],...
            'Color','blue','LineWidth',1);
        set(handles.hsatsList(15),'XData',alfa*x(k),'YData',y(k),'ZData',alfa*x(k));
        k10 = 2;
    end
    if k14==u2
        handles.htray(k) = line([-alfa*x(u2-1) -alfa*x(1)],[y(u2-1) y(1)],[alfa*x(u2-1) alfa*x(1)],...
            'Color','blue','LineWidth',1);
        set(handles.hsatsList(16),'XData',-alfa*x(1),'YData',y(1),'ZData',alfa*x(1));
        k14 = 2;
    end
    if k18==u2
        handles.htray(k) = line([y(u2-1) y(1)],[alfa*x(u2-1) alfa*x(1)],[alfa*x(u2-1) alfa*x(1)],...
            'Color','blue','LineWidth',1);
        set(handles.hsatsList(17),'XData',y(1),'YData',alfa*x(1),'ZData',alfa*x(1));
        k18 = 2;
    end
    if k22==u2
        handles.htray(k) = line([y(u2-1) y(1)],[-alfa*x(u2-1) -alfa*x(1)],[alfa*x(u2-1) alfa*x(1)],...
            'Color','blue','LineWidth',1);
        set(handles.hsatsList(18),'XData',y(1),'YData',-alfa*x(1),'ZData',alfa*x(1));
        k22 = 2;
    end

    %4. satelit u svakoj od odrbita (k3, k7, k11, k15, k19)
    if k3==u2
        handles.htray(k) = line([x(u2-1) x(1)],[y(u2-1) y(1)],[z(u2-1) z(1)],...
            'Color','blue','LineWidth',1);
        set(handles.hsatsList(19),'XData',x(1),'YData',y(1),'ZData',z(1));
        k3 = 2;
    end
    if k7==u2
        handles.htray(k) = line([z(u2-1) z(1)],[y(u2-1) y(1)],[x(u2-1) x(1)],...
            'Color','blue','LineWidth',1);
        set(handles.hsatsList(20),'XData',z(1),'YData',y(1),'ZData',x(1));
        k7 = 2;
    end
    if k11==u2
        handles.htray(k) = line([alfa*x(u2-1) alfa*x(1)],[y(u2-1) y(1)],[alfa*x(u2-1) alfa*x(1)],...
            'Color','blue','LineWidth',1);
        set(handles.hsatsList(21),'XData',alfa*x(k),'YData',y(k),'ZData',alfa*x(k));
        k11 = 2;
    end
    if k15==u2
        handles.htray(k) = line([-alfa*x(u2-1) -alfa*x(1)],[y(u2-1) y(1)],[alfa*x(u2-1) alfa*x(1)],...
            'Color','blue','LineWidth',1);
        set(handles.hsatsList(22),'XData',-alfa*x(1),'YData',y(1),'ZData',alfa*x(1));
        k15 = 2;
    end
    if k19==u2
        handles.htray(k) = line([y(u2-1) y(1)],[alfa*x(u2-1) alfa*x(1)],[alfa*x(u2-1) alfa*x(1)],...
            'Color','blue','LineWidth',1);
        set(handles.hsatsList(23),'XData',y(1),'YData',alfa*x(1),'ZData',alfa*x(1));
        k19 = 2;
    end
    if k23==u2
        handles.htray(k) = line([y(u2-1) y(1)],[-alfa*x(u2-1) -alfa*x(1)],[alfa*x(u2-1) alfa*x(1)],...
            'Color','blue','LineWidth',1);
        set(handles.hsatsList(24),'XData',y(1),'YData',-alfa*x(1),'ZData',alfa*x(1));
        k23 = 2;
    end


    fprintf('\n$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n');
    xdata = get(handles.receiver, 'XData');
    ydata = get(handles.receiver, 'YData');
    zdata = get(handles.receiver, 'ZData');
    reciverPositions = [xdata ydata zdata];

    for ii=1:sateliteNumber;
        delete(linija(ii));
    end

    sateliteNumber = 0;
    matOfVisSatelites = [];
    for i=1:24
        xdataSat = get(handles.hsatsList(i), 'XData');
        ydataSat = get(handles.hsatsList(i), 'YData');
        zdataSat = get(handles.hsatsList(i), 'ZData');
        lineVector = [xdataSat-xdata ydataSat-ydata zdataSat-zdata];
        planeVector = [2*xdata 2*ydata 2*zdata];
        sinus = (abs(sum(lineVector.*planeVector))/(sqrt(sum(lineVector.^2))*sqrt(sum(planeVector.^2))));
        arcussin = asind(sinus);
        planeFormula = 2*xdata*(xdataSat-xdata) + 2*ydata*(ydataSat-ydata) + 2*zdata*(zdataSat-zdata);
        if ((planeFormula > 0) && (arcussin >= 15))
            sateliteNumber = sateliteNumber + 1;
            set(handles.hsatsList(i),'MarkerFaceColor','black');
            linija(sateliteNumber) = line([xdata xdataSat],[ydata ydataSat],[zdata zdataSat],...
                'Color','black','LineWidth',1,'LineStyle','- -');
            distance = sqrt((xdataSat - xdata).^2 + (ydataSat - ydata).^2 + (zdataSat - zdata).^2);
            matOfVisSatelites = [matOfVisSatelites; xdataSat ydataSat zdataSat distance];
        else
            set(handles.hsatsList(i),'MarkerFaceColor','r');
        end
    end

    % MNK - racunanje koordinata
    [Xmnk, Td] = mnk(sateliteNumber, matOfVisSatelites);

    % Njutn - racunanje koordinata
    xstart = 0;
    ystart = 0;
    zstart = 0;
    dstart = 0;
    Xnewthon = NewthonMethod( xstart,ystart,zstart,dstart,sateliteNumber,matOfVisSatelites );
    C = 3*10^8;
    Xnewthon(4) = Xnewthon(4)*C;

    % Ispisuje u konzoli rezultate
    fprintf('\n')
    print1 = ['True GPS receiver coordinates: '];
    printX = ['X: ',num2str(reciverPositions(1))];
    printY = ['Y: ',num2str(reciverPositions(2))];
    printZ = ['Z: ',num2str(reciverPositions(3))];
    disp(print1)
    fprintf('\n')
    disp(printX)
    %   fprintf('\n')
    disp(printY)
    %   fprintf('\n')
    disp(printZ)
    fprintf('\n')
    fprintf('-----------------------------------')
    fprintf('\n')
    print2 = ['Number of visible satellites: ',num2str(sateliteNumber)];
    disp(print2)
    fprintf('\n')
    fprintf('-----------------------------------')

    % MNK
    fprintf('\n')
    print3 = ['MNK GPS receiver coordinates: '];

%     printXmnk = ['Xmnk: ',num2str(Xmnk(1))];
%     printYmnk = ['Ymnk: ',num2str(Xmnk(2))];
%     printZmnk = ['Zmnk: ',num2str(Xmnk(3))];
    disp(print3)
    Xmnk
    Td
    fprintf('\n')
%     disp(printXmnk)
%     %fprintf('\n')
%     disp(printYmnk)
%     %fprintf('\n')
%     disp(printZmnk)
    fprintf('\n')
    fprintf('-----------------------------------')

    %Njutn
    fprintf('\n')
    print4 = ['Newthon GPS receiver coordinates: '];

%     printXmnk = ['Xmnk: ',num2str(Xmnk(1))];
%     printYmnk = ['Ymnk: ',num2str(Xmnk(2))];
%     printZmnk = ['Zmnk: ',num2str(Xmnk(3))];
    disp(print4)
    Xnewthon
    fprintf('\n')
%     disp(printXmnk)
%     %fprintf('\n')
%     disp(printYmnk)
%     %fprintf('\n')
%     disp(printZmnk)
    fprintf('\n')
    fprintf('-----------------------------------')

    rotate(handles.receiver,[0,1,1],5);

    rotate(sh,[0,1,1],5);
    drawnow;
    %   pause(1);

end
